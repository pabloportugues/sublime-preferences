# Sublime Text

## A repository to store the Sublime Text preferences

### How to install

- `brew cask install sublime-text` to install Sublime
- Create the packages folder
	- If `~/Library/Application\ Support/Sublime\ Text\ 3` does not exist:
		- `mkdir ~/Library/Application\ Support/Sublime\ Text\ 3` to create the packages folder
	- if not
		- `cd ~/Library/Application\ Support/Sublime\ Text\ 3 && rm -rf *` - remove existing settings
- Open and close Sublime Text (to create default folders)
- `git clone git@gitlab.com:bytemybits/sublime-preferences.git Packages/User && cd Packages/User`
-  `vi Preferences.sublime-settings` and comment out the `color_scheme` so that Sublime doesn't throw errors because the Package Control isn't installed yet
- Open Sublime Text and install Package Control (*cmd/ctrl + shift + p*)
- Wait for a minute while packages are installed (you can check *list packages* to see them being installed)
- Restart Sublime Text
- Uncomment the color scheme you just commented a few steps back
- Enjoy!